package io.jibit.recruitment.repository;

import io.jibit.recruitment.domain.OrderActivity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrderActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderActivityRepository extends JpaRepository<OrderActivity, Long> {

}
