package io.jibit.recruitment.repository;

import io.jibit.recruitment.domain.Order;

import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Order entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("from Order o where o.id = :id")
	Optional<Order> findOrderForWrite(Long id);

}
