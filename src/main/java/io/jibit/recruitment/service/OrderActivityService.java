package io.jibit.recruitment.service;

import io.jibit.recruitment.service.dto.OrderActivityDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing OrderActivity.
 */
public interface OrderActivityService {

    /**
     * Save a orderActivity.
     *
     * @param orderActivityDTO the entity to save
     * @return the persisted entity
     */
    OrderActivityDTO save(OrderActivityDTO orderActivityDTO);

    /**
     * Get all the orderActivities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OrderActivityDTO> findAll(Pageable pageable);


    /**
     * Get the "id" orderActivity.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OrderActivityDTO> findOne(Long id);

    /**
     * Delete the "id" orderActivity.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
