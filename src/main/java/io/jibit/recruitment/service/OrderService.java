package io.jibit.recruitment.service;

import io.jibit.recruitment.service.dto.OrderDTO;
import io.jibit.recruitment.service.dto.OrderUpdateDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Order.
 */
public interface OrderService {

    /**
     * Save a order.
     *
     * @param orderDTO the entity to save
     * @return the persisted entity
     */
    OrderDTO save(OrderDTO orderDTO);
    
    /**
     * Update a order.
     *
     * @param orderUpdateDTO the entity to update
     * @return the persisted entity
     */
    OrderDTO update(OrderUpdateDTO orderUpdateDTO);

    /**
     * Register payment successful for an Order
     *
     * @param orderId id of the target Order
     * @return the updated Order entity
     */
    OrderDTO registerPaymentSuccess(Long orderId);

    /**
     * cancel an Order
     *
     * @param orderId id of the target Order
     * @return the updated Order entity
     */
    OrderDTO cancelOrder(Long orderId);

    /**
     * Get all the orders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OrderDTO> findAll(Pageable pageable);


    /**
     * Get the "id" order.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OrderDTO> findOne(Long id);

    /**
     * Delete the "id" order.
     *
     * @param id the id of the entity
     */
    void delete(Long id);


}
