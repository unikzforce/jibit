package io.jibit.recruitment.service.impl;

import io.jibit.recruitment.service.OrderActivityService;
import io.jibit.recruitment.domain.OrderActivity;
import io.jibit.recruitment.repository.OrderActivityRepository;
import io.jibit.recruitment.service.dto.OrderActivityDTO;
import io.jibit.recruitment.service.mapper.OrderActivityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing OrderActivity.
 */
@Service
@Transactional
public class OrderActivityServiceImpl implements OrderActivityService {

    private final Logger log = LoggerFactory.getLogger(OrderActivityServiceImpl.class);

    private final OrderActivityRepository orderActivityRepository;

    private final OrderActivityMapper orderActivityMapper;

    public OrderActivityServiceImpl(OrderActivityRepository orderActivityRepository, OrderActivityMapper orderActivityMapper) {
        this.orderActivityRepository = orderActivityRepository;
        this.orderActivityMapper = orderActivityMapper;
    }

    /**
     * Save a orderActivity.
     *
     * @param orderActivityDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OrderActivityDTO save(OrderActivityDTO orderActivityDTO) {
        log.debug("Request to save OrderActivity : {}", orderActivityDTO);
        OrderActivity orderActivity = orderActivityMapper.toEntity(orderActivityDTO);
        orderActivity = orderActivityRepository.save(orderActivity);
        return orderActivityMapper.toDto(orderActivity);
    }

    /**
     * Get all the orderActivities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrderActivityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderActivities");
        return orderActivityRepository.findAll(pageable)
            .map(orderActivityMapper::toDto);
    }


    /**
     * Get one orderActivity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrderActivityDTO> findOne(Long id) {
        log.debug("Request to get OrderActivity : {}", id);
        return orderActivityRepository.findById(id)
            .map(orderActivityMapper::toDto);
    }

    /**
     * Delete the orderActivity by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderActivity : {}", id);        orderActivityRepository.deleteById(id);
    }
}
