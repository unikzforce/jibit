package io.jibit.recruitment.service.impl;

import io.jibit.recruitment.service.OrderService;
import io.jibit.recruitment.domain.Order;
import io.jibit.recruitment.domain.OrderActivity;
import io.jibit.recruitment.domain.enumeration.ActivityType;
import io.jibit.recruitment.domain.enumeration.OrderStatus;
import io.jibit.recruitment.repository.OrderActivityRepository;
import io.jibit.recruitment.repository.OrderRepository;
import io.jibit.recruitment.service.dto.OrderDTO;
import io.jibit.recruitment.service.dto.OrderUpdateDTO;
import io.jibit.recruitment.service.mapper.OrderMapper;
import io.jibit.recruitment.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing Order.
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    private final OrderRepository orderRepository;
    
    private final OrderActivityRepository orderActivityRepository;

    private final OrderMapper orderMapper;
    
    private static final String ENTITY_NAME = "recruitmentTestOrder";

    public OrderServiceImpl(OrderRepository orderRepository, OrderMapper orderMapper, OrderActivityRepository orderActivityRepository) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.orderActivityRepository = orderActivityRepository;
    }

    /**
     * Save a order.
     *
     * @param orderDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OrderDTO save(OrderDTO orderDTO) {
        log.debug("Request to save Order : {}", orderDTO);
        Order order = orderMapper.toEntity(orderDTO);
        ZonedDateTime now = ZonedDateTime.now();
        
        order.setCreationTime(now);
        order.setStatus(OrderStatus.INITIALIZED);
        order.setPaymentTime(null);
        
        order = orderRepository.save(order);
        
        OrderActivity creationActivity = new OrderActivity();
        creationActivity.setOrder(order);
        creationActivity.setActivityTime(now);
        creationActivity.setActivityType(ActivityType.CREATION);
        
        orderActivityRepository.save(creationActivity);
        
        return orderMapper.toDto(order);
    }
    
    /**
     * Update a order.
     *
     * @param orderUpdateDTO the entity to update
     * @return the persisted entity
     */
    @Override
    public OrderDTO update(OrderUpdateDTO orderUpdateDTO) {
        log.debug("Request to save Order : {}", orderUpdateDTO);
    	Optional<Order> orderOpt = orderRepository.findOrderForWrite(orderUpdateDTO.getId());
    	if(orderOpt.isPresent() == false )
    		throw new BadRequestAlertException("No such order exists to update information for.", ENTITY_NAME, ENTITY_NAME+"_update_order_not_found");

    	Order order = orderOpt.get();
    	if(order.getStatus() != OrderStatus.INITIALIZED )
    		throw new BadRequestAlertException("Unable to update order which is not in initialized state.", ENTITY_NAME, ENTITY_NAME+"_update_not_in_initialized_state");
    	
    	OrderActivity updateActivity = new OrderActivity();
    	updateActivity.setOrder(order);
    	updateActivity.setActivityTime(ZonedDateTime.now());
    	updateActivity.setOldAmount(order.getAmount());
    	updateActivity.setNewAmount(orderUpdateDTO.getAmount());
    	updateActivity.setOldItemName(order.getItemName());
    	updateActivity.setNewItemName(orderUpdateDTO.getItemName());
    	updateActivity.setActivityType(ActivityType.UPDATING);
    	
    	order.setAmount(orderUpdateDTO.getAmount());
    	order.setItemName(orderUpdateDTO.getItemName());
    	
    	orderActivityRepository.save(updateActivity);
    	order = orderRepository.save(order);

        return orderMapper.toDto(order);
    }
    
    /**
     * Register payment successful for an Order
     *
     * @param orderId id of the target Order
     * @return the updated Order entity
     */
    public OrderDTO registerPaymentSuccess(Long orderId) {
    	Optional<Order> orderOpt = orderRepository.findOrderForWrite(orderId);
    	if(orderOpt.isPresent() == false )
    		throw new BadRequestAlertException("No such order exists to register payment success for.", ENTITY_NAME, ENTITY_NAME+"_registerPayment_order_not_found");

    	Order order = orderOpt.get();
    	if(order.getStatus() != OrderStatus.INITIALIZED)
    		throw new BadRequestAlertException("Order must be in initialized state to register payment success for it.", ENTITY_NAME, ENTITY_NAME+"_registerPayment_order_not_in_initialized_state");

    	ZonedDateTime now = ZonedDateTime.now();
    	
    	OrderActivity paymentActivity = new OrderActivity();
    	
    	paymentActivity.setActivityTime(now);
    	paymentActivity.setActivityType(ActivityType.PAYMENT_SUCCESS);
    	paymentActivity.setOrder(order);
    	paymentActivity.setFromStatus(order.getStatus());
    	paymentActivity.setToStatus(OrderStatus.PAID);
    	order.setStatus(OrderStatus.PAID);
    	order.setPaymentTime(now);
    	
    	order = orderRepository.save(order);
    	orderActivityRepository.save(paymentActivity);
    	return orderMapper.toDto(order);
    }
    
    /**
     * cancel an Order
     *
     * @param orderId id of the target Order
     * @return the updated Order entity
     */
    @Override
    public OrderDTO cancelOrder(Long orderId) {
    	Optional<Order> orderOpt = orderRepository.findOrderForWrite(orderId);
    	
    	if(orderOpt.isPresent() == false )
    		throw new BadRequestAlertException("No such order exists to cancel.", ENTITY_NAME, ENTITY_NAME+"_cancel_order_not_found");
 
    	Order order = orderOpt.get();
    	
    	if(order.getStatus() != OrderStatus.INITIALIZED )
    		throw new BadRequestAlertException("Unable to cancel order which is not in initialized state.", ENTITY_NAME, ENTITY_NAME+"_cancel_not_in_initialized_state");
    	
    	order.setStatus(OrderStatus.CANCELED_BY_CUSTOMER);

    	OrderActivity cancelActivity = new OrderActivity();
    	cancelActivity.setActivityTime(ZonedDateTime.now());
    	cancelActivity.setActivityType(ActivityType.CANCELING);
    	cancelActivity.setFromStatus(OrderStatus.INITIALIZED);
    	cancelActivity.setToStatus(OrderStatus.CANCELED_BY_CUSTOMER);
    	cancelActivity.setOrder(order);
    	
    	order = orderRepository.save(order);
    	orderActivityRepository.save(cancelActivity);
    	
    	return orderMapper.toDto(order);
    }

    /**
     * Get all the orders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Orders");
        return orderRepository.findAll(pageable)
            .map(orderMapper::toDto);
    }


    /**
     * Get one order by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrderDTO> findOne(Long id) {
        log.debug("Request to get Order : {}", id);
        return orderRepository.findById(id)
            .map(orderMapper::toDto);
    }

    /**
     * Delete the order by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Order : {}", id);        orderRepository.deleteById(id);
    }
}
