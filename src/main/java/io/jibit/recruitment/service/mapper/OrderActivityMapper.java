package io.jibit.recruitment.service.mapper;

import io.jibit.recruitment.domain.*;
import io.jibit.recruitment.service.dto.OrderActivityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OrderActivity and its DTO OrderActivityDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderMapper.class})
public interface OrderActivityMapper extends EntityMapper<OrderActivityDTO, OrderActivity> {

    @Mapping(source = "order.id", target = "orderId")
    OrderActivityDTO toDto(OrderActivity orderActivity);

    @Mapping(source = "orderId", target = "order")
    OrderActivity toEntity(OrderActivityDTO orderActivityDTO);

    default OrderActivity fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderActivity orderActivity = new OrderActivity();
        orderActivity.setId(id);
        return orderActivity;
    }
}
