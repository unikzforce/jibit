package io.jibit.recruitment.service.mapper;

import io.jibit.recruitment.domain.*;
import io.jibit.recruitment.service.dto.OrderDTO;
import io.jibit.recruitment.service.dto.OrderUpdateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Order and its DTO OrderDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {

	OrderUpdateDTO toUpdateDto(Order order);

    default Order fromId(Long id) {
        if (id == null) {
            return null;
        }
        Order order = new Order();
        order.setId(id);
        return order;
    }
}
