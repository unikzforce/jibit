package io.jibit.recruitment.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import io.jibit.recruitment.domain.enumeration.OrderStatus;
import io.jibit.recruitment.domain.enumeration.OrderStatus;
import io.jibit.recruitment.domain.enumeration.ActivityType;

/**
 * A DTO for the OrderActivity entity.
 */
public class OrderActivityDTO implements Serializable {

    private Long id;

    private OrderStatus fromStatus;

    private OrderStatus toStatus;

    private ZonedDateTime activityTime;

    private ActivityType activityType;

    private Double oldAmount;

    private Double newAmount;

    private String oldItemName;

    private String newItemName;

    private Long orderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(OrderStatus fromStatus) {
        this.fromStatus = fromStatus;
    }

    public OrderStatus getToStatus() {
        return toStatus;
    }

    public void setToStatus(OrderStatus toStatus) {
        this.toStatus = toStatus;
    }

    public ZonedDateTime getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(ZonedDateTime activityTime) {
        this.activityTime = activityTime;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Double getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(Double oldAmount) {
        this.oldAmount = oldAmount;
    }

    public Double getNewAmount() {
        return newAmount;
    }

    public void setNewAmount(Double newAmount) {
        this.newAmount = newAmount;
    }

    public String getOldItemName() {
        return oldItemName;
    }

    public void setOldItemName(String oldItemName) {
        this.oldItemName = oldItemName;
    }

    public String getNewItemName() {
        return newItemName;
    }

    public void setNewItemName(String newItemName) {
        this.newItemName = newItemName;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderActivityDTO orderActivityDTO = (OrderActivityDTO) o;
        if (orderActivityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderActivityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderActivityDTO{" +
            "id=" + getId() +
            ", fromStatus='" + getFromStatus() + "'" +
            ", toStatus='" + getToStatus() + "'" +
            ", activityTime='" + getActivityTime() + "'" +
            ", activityType='" + getActivityType() + "'" +
            ", oldAmount=" + getOldAmount() +
            ", newAmount=" + getNewAmount() +
            ", oldItemName='" + getOldItemName() + "'" +
            ", newItemName='" + getNewItemName() + "'" +
            ", order=" + getOrderId() +
            "}";
    }
}
