package io.jibit.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import io.jibit.recruitment.domain.enumeration.OrderStatus;

import io.jibit.recruitment.domain.enumeration.ActivityType;

/**
 * A OrderActivity.
 */
@Entity
@Table(name = "order_activity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "from_status")
    private OrderStatus fromStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "to_status")
    private OrderStatus toStatus;

    @Column(name = "activity_time")
    private ZonedDateTime activityTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "activity_type")
    private ActivityType activityType;

    @Column(name = "old_amount")
    private Double oldAmount;

    @Column(name = "new_amount")
    private Double newAmount;

    @Column(name = "old_item_name")
    private String oldItemName;

    @Column(name = "new_item_name")
    private String newItemName;

    @ManyToOne
    @JsonIgnoreProperties("orderActivities")
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getFromStatus() {
        return fromStatus;
    }

    public OrderActivity fromStatus(OrderStatus fromStatus) {
        this.fromStatus = fromStatus;
        return this;
    }

    public void setFromStatus(OrderStatus fromStatus) {
        this.fromStatus = fromStatus;
    }

    public OrderStatus getToStatus() {
        return toStatus;
    }

    public OrderActivity toStatus(OrderStatus toStatus) {
        this.toStatus = toStatus;
        return this;
    }

    public void setToStatus(OrderStatus toStatus) {
        this.toStatus = toStatus;
    }

    public ZonedDateTime getActivityTime() {
        return activityTime;
    }

    public OrderActivity activityTime(ZonedDateTime activityTime) {
        this.activityTime = activityTime;
        return this;
    }

    public void setActivityTime(ZonedDateTime activityTime) {
        this.activityTime = activityTime;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public OrderActivity activityType(ActivityType activityType) {
        this.activityType = activityType;
        return this;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Double getOldAmount() {
        return oldAmount;
    }

    public OrderActivity oldAmount(Double oldAmount) {
        this.oldAmount = oldAmount;
        return this;
    }

    public void setOldAmount(Double oldAmount) {
        this.oldAmount = oldAmount;
    }

    public Double getNewAmount() {
        return newAmount;
    }

    public OrderActivity newAmount(Double newAmount) {
        this.newAmount = newAmount;
        return this;
    }

    public void setNewAmount(Double newAmount) {
        this.newAmount = newAmount;
    }

    public String getOldItemName() {
        return oldItemName;
    }

    public OrderActivity oldItemName(String oldItemName) {
        this.oldItemName = oldItemName;
        return this;
    }

    public void setOldItemName(String oldItemName) {
        this.oldItemName = oldItemName;
    }

    public String getNewItemName() {
        return newItemName;
    }

    public OrderActivity newItemName(String newItemName) {
        this.newItemName = newItemName;
        return this;
    }

    public void setNewItemName(String newItemName) {
        this.newItemName = newItemName;
    }

    public Order getOrder() {
        return order;
    }

    public OrderActivity order(Order order) {
        this.order = order;
        return this;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderActivity orderActivity = (OrderActivity) o;
        if (orderActivity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderActivity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderActivity{" +
            "id=" + getId() +
            ", fromStatus='" + getFromStatus() + "'" +
            ", toStatus='" + getToStatus() + "'" +
            ", activityTime='" + getActivityTime() + "'" +
            ", activityType='" + getActivityType() + "'" +
            ", oldAmount=" + getOldAmount() +
            ", newAmount=" + getNewAmount() +
            ", oldItemName='" + getOldItemName() + "'" +
            ", newItemName='" + getNewItemName() + "'" +
            "}";
    }
}
