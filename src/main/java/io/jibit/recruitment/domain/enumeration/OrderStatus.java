package io.jibit.recruitment.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    INITIALIZED, PAID, FAILED, CANCELED_BY_CUSTOMER
}
