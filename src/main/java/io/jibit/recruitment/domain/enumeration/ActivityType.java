package io.jibit.recruitment.domain.enumeration;

/**
 * The ActivityType enumeration.
 */
public enum ActivityType {
    PAYMENT_SUCCESS, CANCELING, FAILURE, CREATION, UPDATING
}
