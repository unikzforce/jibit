package io.jibit.recruitment.web.rest;

import io.jibit.recruitment.RecruitmentTestApp;

import io.jibit.recruitment.domain.Order;
import io.jibit.recruitment.domain.OrderActivity;
import io.jibit.recruitment.repository.OrderActivityRepository;
import io.jibit.recruitment.repository.OrderRepository;
import io.jibit.recruitment.service.OrderService;
import io.jibit.recruitment.service.dto.OrderDTO;
import io.jibit.recruitment.service.dto.OrderUpdateDTO;
import io.jibit.recruitment.service.mapper.OrderMapper;
import io.jibit.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static io.jibit.recruitment.web.rest.TestUtil.sameInstant;
import static io.jibit.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.jibit.recruitment.domain.enumeration.ActivityType;
import io.jibit.recruitment.domain.enumeration.OrderStatus;
/**
 * Test class for the OrderResource REST controller.
 *
 * @see OrderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecruitmentTestApp.class)
public class OrderResourceIntTest {

    private static final OrderStatus DEFAULT_STATUS = OrderStatus.INITIALIZED;
    private static final OrderStatus UPDATED_STATUS = OrderStatus.PAID;

    private static final ZonedDateTime DEFAULT_CREATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_PAYMENT_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PAYMENT_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_MERCHANT = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT = "BBBBBBBBBB";

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_ITEM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_NAME = "BBBBBBBBBB";

    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderActivityRepository orderActivityRepository;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderService orderService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrderMockMvc;

    private Order order;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderResource orderResource = new OrderResource(orderService);
        this.restOrderMockMvc = MockMvcBuilders.standaloneSetup(orderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Order createEntity(EntityManager em) {
        Order order = new Order()
            .status(DEFAULT_STATUS)
            .creationTime(DEFAULT_CREATION_TIME)
            .paymentTime(DEFAULT_PAYMENT_TIME)
            .merchant(DEFAULT_MERCHANT)
            .amount(DEFAULT_AMOUNT)
            .itemName(DEFAULT_ITEM_NAME);
        return order;
    }

    @Before
    public void initTest() {
        order = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrder() throws Exception {
        int orderTableSizeBeforeCreate = orderRepository.findAll().size();
        int activityTableSizeBeforeCreate = orderActivityRepository.findAll().size();

        // Create the Order
        OrderDTO orderDTO = orderMapper.toDto(order);
        restOrderMockMvc.perform(post("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isCreated());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(orderTableSizeBeforeCreate + 1);
        
        List<OrderActivity> activityList = orderActivityRepository.findAll();
        assertThat(activityList).hasSize(activityTableSizeBeforeCreate + 1);
        
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.INITIALIZED);
//        assertThat(testOrder.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
//        assertThat(testOrder.getPaymentTime()).isEqualTo(DEFAULT_PAYMENT_TIME);
        assertThat(testOrder.getMerchant()).isEqualTo(DEFAULT_MERCHANT);
        assertThat(testOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testOrder.getItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        
        
        OrderActivity testActivity = activityList.get(activityList.size() - 1);
        assertThat(testActivity.getActivityType()).isEqualTo(ActivityType.CREATION);
        assertThat(testActivity.getActivityTime()).isEqualTo(testOrder.getCreationTime());
        assertThat(testActivity.getOrder().getId()).isEqualTo(testOrder.getId());
    }

    @Test
    @Transactional
    public void createOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderRepository.findAll().size();

        // Create the Order with an existing ID
        order.setId(1L);
        OrderDTO orderDTO = orderMapper.toDto(order);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderMockMvc.perform(post("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeCreate);
    }
    
    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get all the orderList
        restOrderMockMvc.perform(get("/api/orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(order.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(sameInstant(DEFAULT_CREATION_TIME))))
            .andExpect(jsonPath("$.[*].paymentTime").value(hasItem(sameInstant(DEFAULT_PAYMENT_TIME))))
            .andExpect(jsonPath("$.[*].merchant").value(hasItem(DEFAULT_MERCHANT.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].itemName").value(hasItem(DEFAULT_ITEM_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", order.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(order.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.creationTime").value(sameInstant(DEFAULT_CREATION_TIME)))
            .andExpect(jsonPath("$.paymentTime").value(sameInstant(DEFAULT_PAYMENT_TIME)))
            .andExpect(jsonPath("$.merchant").value(DEFAULT_MERCHANT.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.itemName").value(DEFAULT_ITEM_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOrder() throws Exception {
        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        int orderTableSizeBeforeUpdate = orderRepository.findAll().size();
        int activityTableSizeBeforeUpdate = orderActivityRepository.findAll().size();
        

        // Update the order
        Order updatedOrder = orderRepository.findById(order.getId()).get();
        // Disconnect from session so that the updates on updatedOrder are not directly saved in db
        em.detach(updatedOrder);
        updatedOrder
            .amount(UPDATED_AMOUNT)
            .itemName(UPDATED_ITEM_NAME);
        OrderUpdateDTO orderUpdateDTO = orderMapper.toUpdateDto(updatedOrder);

        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderUpdateDTO)))
            .andExpect(status().isOk());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(orderTableSizeBeforeUpdate);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.INITIALIZED);
        assertThat(testOrder.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testOrder.getItemName()).isEqualTo(UPDATED_ITEM_NAME);
        
        List<OrderActivity> activityList = orderActivityRepository.findAll();
        assertThat(activityList).hasSize(activityTableSizeBeforeUpdate + 1);
        OrderActivity testActivity = activityList.get(activityList.size() - 1);
        assertThat(testActivity.getActivityType()).isEqualTo(ActivityType.UPDATING);
        assertThat(testActivity.getOldAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testActivity.getNewAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testActivity.getOldItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        assertThat(testActivity.getNewItemName()).isEqualTo(UPDATED_ITEM_NAME);
        assertThat(testActivity.getOrder().getId()).isEqualTo(order.getId());
    }
    
    @Test
    @Transactional
    public void updateAlreadyPaidOrder() throws Exception {
        // Initialize the database
    	order.setStatus(OrderStatus.PAID);
        orderRepository.saveAndFlush(order);

        int orderTableSizeBeforeUpdate = orderRepository.findAll().size();
        int activityTableSizeBeforeUpdate = orderActivityRepository.findAll().size();
        

        // Update the order
        Order updatedOrder = orderRepository.findById(order.getId()).get();
        // Disconnect from session so that the updates on updatedOrder are not directly saved in db
        em.detach(updatedOrder);
        updatedOrder
            .amount(UPDATED_AMOUNT)
            .itemName(UPDATED_ITEM_NAME);
        OrderDTO orderDTO = orderMapper.toDto(updatedOrder);

        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(orderTableSizeBeforeUpdate);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.PAID);
        assertThat(testOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testOrder.getPaymentTime()).isEqualTo(DEFAULT_PAYMENT_TIME);
        assertThat(testOrder.getMerchant()).isEqualTo(DEFAULT_MERCHANT);
        assertThat(testOrder.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testOrder.getItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        
        List<OrderActivity> activityList = orderActivityRepository.findAll();
        assertThat(activityList).hasSize(activityTableSizeBeforeUpdate);
    }
    
    @Test
    @Transactional
    public void updateAlreadyFailedOrder() throws Exception {
        // Initialize the database
    	order.setStatus(OrderStatus.FAILED);
        orderRepository.saveAndFlush(order);

        int orderTableSizeBeforeUpdate = orderRepository.findAll().size();
        int activityTableSizeBeforeUpdate = orderActivityRepository.findAll().size();
        

        // Update the order
        Order updatedOrder = orderRepository.findById(order.getId()).get();
        // Disconnect from session so that the updates on updatedOrder are not directly saved in db
        em.detach(updatedOrder);
        updatedOrder
            .amount(UPDATED_AMOUNT)
            .itemName(UPDATED_ITEM_NAME);
        OrderDTO orderDTO = orderMapper.toDto(updatedOrder);

        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(orderTableSizeBeforeUpdate);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.FAILED);
        assertThat(testOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testOrder.getPaymentTime()).isEqualTo(DEFAULT_PAYMENT_TIME);
        assertThat(testOrder.getMerchant()).isEqualTo(DEFAULT_MERCHANT);
        assertThat(testOrder.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testOrder.getItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        
        List<OrderActivity> activityList = orderActivityRepository.findAll();
        assertThat(activityList).hasSize(activityTableSizeBeforeUpdate);
    }
    
    @Test
    @Transactional
    public void updateAlreadyCanceledOrder() throws Exception {
        // Initialize the database
    	order.setStatus(OrderStatus.CANCELED_BY_CUSTOMER);
        orderRepository.saveAndFlush(order);

        int orderTableSizeBeforeUpdate = orderRepository.findAll().size();
        int activityTableSizeBeforeUpdate = orderActivityRepository.findAll().size();
        

        // Update the order
        Order updatedOrder = orderRepository.findById(order.getId()).get();
        // Disconnect from session so that the updates on updatedOrder are not directly saved in db
        em.detach(updatedOrder);
        updatedOrder
            .amount(UPDATED_AMOUNT)
            .itemName(UPDATED_ITEM_NAME);
        OrderDTO orderDTO = orderMapper.toDto(updatedOrder);

        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(orderTableSizeBeforeUpdate);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.CANCELED_BY_CUSTOMER);
        assertThat(testOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testOrder.getPaymentTime()).isEqualTo(DEFAULT_PAYMENT_TIME);
        assertThat(testOrder.getMerchant()).isEqualTo(DEFAULT_MERCHANT);
        assertThat(testOrder.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testOrder.getItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        
        List<OrderActivity> activityList = orderActivityRepository.findAll();
        assertThat(activityList).hasSize(activityTableSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateNonExistingOrder() throws Exception {
        int databaseSizeBeforeUpdate = orderRepository.findAll().size();

        // Create the Order
        OrderDTO orderDTO = orderMapper.toDto(order);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeUpdate);
    }
    
    @Test
    @Transactional
    public void registerPaymentSuccessForInitializedOrder() throws Exception {
    	order.setPaymentTime(null);
    	order.setStatus(OrderStatus.INITIALIZED);
    
    	// Initialize the database
    	orderRepository.saveAndFlush(order);
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	MvcResult result = restOrderMockMvc.perform(put("/api/orders/{id}/register-payment-success", order.getId())
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
//			.andDo(MockMvcResultHandlers.print())
          	.andExpect(status().isOk()).andReturn();
    	
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
        Order testOrder = orderList.get(orderList.size() - 1);
    	assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.PAID);
    	assertThat(testOrder.getPaymentTime()).isNotNull();
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess + 1);
    	OrderActivity testActivity = activityList.get(activityList.size() - 1);
    	assertThat(testActivity.getFromStatus()).isEqualTo(OrderStatus.INITIALIZED);
    	assertThat(testActivity.getToStatus()).isEqualTo(OrderStatus.PAID);
    	
    }
    
    @Test
    @Transactional
    public void registerPaymentForNonExistentOrder() throws Exception {
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	restOrderMockMvc.perform(put("/api/orders/{id}/register-payment-success", Long.MAX_VALUE)
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
          	.andExpect(status().isBadRequest());
  
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess);
    	
    }
    
    @Test
    @Transactional
    public void registerPaymentForNonInitializedOrder() throws Exception {
    	
    	order.setPaymentTime(null);
    	order.setStatus(OrderStatus.FAILED);
    
    	// Initialize the database
    	orderRepository.saveAndFlush(order);
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	restOrderMockMvc.perform(put("/api/orders/{id}/register-payment-success", order.getId())
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
          	.andExpect(status().isBadRequest());
  
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess);
    	
    }
    
    @Test
    @Transactional
    public void cancelInitializedOrder() throws Exception {
    	order.setPaymentTime(null);
    	order.setStatus(OrderStatus.INITIALIZED);
    
    	// Initialize the database
    	orderRepository.saveAndFlush(order);
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	MvcResult result = restOrderMockMvc.perform(put("/api/orders/{id}/cancel", order.getId())
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
//			.andDo(MockMvcResultHandlers.print())
          	.andExpect(status().isOk()).andReturn();
    	
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
        Order testOrder = orderList.get(orderList.size() - 1);
    	assertThat(testOrder.getStatus()).isEqualTo(OrderStatus.CANCELED_BY_CUSTOMER);
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess + 1);
    	OrderActivity testActivity = activityList.get(activityList.size() - 1);
    	assertThat(testActivity.getFromStatus()).isEqualTo(OrderStatus.INITIALIZED);
    	assertThat(testActivity.getToStatus()).isEqualTo(OrderStatus.CANCELED_BY_CUSTOMER);
    	
    }
    
    @Test
    @Transactional
    public void cancelNonExistentOrder() throws Exception {
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	restOrderMockMvc.perform(put("/api/orders/{id}/cancel", Long.MAX_VALUE)
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
          	.andExpect(status().isBadRequest());
  
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess);
    	
    }
    
    @Test
    @Transactional
    public void cancelNonInitializedOrder() throws Exception {
    	
    	order.setPaymentTime(null);
    	order.setStatus(OrderStatus.PAID);
    
    	// Initialize the database
    	orderRepository.saveAndFlush(order);
    	
        int orderTableBeforeSizeRegisterPaymentSuccess = orderRepository.findAll().size();
        int activityTableSizeBefireRegisterPaymentSuccess = orderActivityRepository.findAll().size();
  
    	restOrderMockMvc.perform(put("/api/orders/{id}/cancel", order.getId())
    		.accept(TestUtil.APPLICATION_JSON_UTF8))
          	.andExpect(status().isBadRequest());
  
    	List<Order> orderList = orderRepository.findAll();
    	assertThat(orderList).hasSize(orderTableBeforeSizeRegisterPaymentSuccess);
    	
    	List<OrderActivity> activityList = orderActivityRepository.findAll();
    	assertThat(activityList).hasSize(activityTableSizeBefireRegisterPaymentSuccess);
    	
    }

//    @Test
//    @Transactional
//    public void deleteOrder() throws Exception {
//        // Initialize the database
//        orderRepository.saveAndFlush(order);
//
//        int databaseSizeBeforeDelete = orderRepository.findAll().size();
//
//        // Delete the order
//        restOrderMockMvc.perform(delete("/api/orders/{id}", order.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        // Validate the database is empty
//        List<Order> orderList = orderRepository.findAll();
//        assertThat(orderList).hasSize(databaseSizeBeforeDelete - 1);
//    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Order.class);
        Order order1 = new Order();
        order1.setId(1L);
        Order order2 = new Order();
        order2.setId(order1.getId());
        assertThat(order1).isEqualTo(order2);
        order2.setId(2L);
        assertThat(order1).isNotEqualTo(order2);
        order1.setId(null);
        assertThat(order1).isNotEqualTo(order2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderDTO.class);
        OrderDTO orderDTO1 = new OrderDTO();
        orderDTO1.setId(1L);
        OrderDTO orderDTO2 = new OrderDTO();
        assertThat(orderDTO1).isNotEqualTo(orderDTO2);
        orderDTO2.setId(orderDTO1.getId());
        assertThat(orderDTO1).isEqualTo(orderDTO2);
        orderDTO2.setId(2L);
        assertThat(orderDTO1).isNotEqualTo(orderDTO2);
        orderDTO1.setId(null);
        assertThat(orderDTO1).isNotEqualTo(orderDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(orderMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(orderMapper.fromId(null)).isNull();
    }
}
