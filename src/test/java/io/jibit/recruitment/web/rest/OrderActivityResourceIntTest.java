package io.jibit.recruitment.web.rest;

import io.jibit.recruitment.RecruitmentTestApp;

import io.jibit.recruitment.domain.OrderActivity;
import io.jibit.recruitment.repository.OrderActivityRepository;
import io.jibit.recruitment.service.OrderActivityService;
import io.jibit.recruitment.service.dto.OrderActivityDTO;
import io.jibit.recruitment.service.mapper.OrderActivityMapper;
import io.jibit.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static io.jibit.recruitment.web.rest.TestUtil.sameInstant;
import static io.jibit.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.jibit.recruitment.domain.enumeration.OrderStatus;
import io.jibit.recruitment.domain.enumeration.OrderStatus;
import io.jibit.recruitment.domain.enumeration.ActivityType;
/**
 * Test class for the OrderActivityResource REST controller.
 *
 * @see OrderActivityResource
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = RecruitmentTestApp.class)
public class OrderActivityResourceIntTest {
//
//    private static final OrderStatus DEFAULT_FROM_STATUS = OrderStatus.INITIALIZED;
//    private static final OrderStatus UPDATED_FROM_STATUS = OrderStatus.PAID;
//
//    private static final OrderStatus DEFAULT_TO_STATUS = OrderStatus.INITIALIZED;
//    private static final OrderStatus UPDATED_TO_STATUS = OrderStatus.PAID;
//
//    private static final ZonedDateTime DEFAULT_ACTIVITY_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
//    private static final ZonedDateTime UPDATED_ACTIVITY_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
//
//    private static final ActivityType DEFAULT_ACTIVITY_TYPE = ActivityType.PAYMENT_SUCCESS;
//    private static final ActivityType UPDATED_ACTIVITY_TYPE = ActivityType.CANCELING;
//
//    private static final Double DEFAULT_OLD_AMOUNT = 1D;
//    private static final Double UPDATED_OLD_AMOUNT = 2D;
//
//    private static final Double DEFAULT_NEW_AMOUNT = 1D;
//    private static final Double UPDATED_NEW_AMOUNT = 2D;
//
//    private static final String DEFAULT_OLD_ITEM_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_OLD_ITEM_NAME = "BBBBBBBBBB";
//
//    private static final String DEFAULT_NEW_ITEM_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_NEW_ITEM_NAME = "BBBBBBBBBB";
//
//    @Autowired
//    private OrderActivityRepository orderActivityRepository;
//
//    @Autowired
//    private OrderActivityMapper orderActivityMapper;
//
//    @Autowired
//    private OrderActivityService orderActivityService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restOrderActivityMockMvc;
//
//    private OrderActivity orderActivity;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final OrderActivityResource orderActivityResource = new OrderActivityResource(orderActivityService);
//        this.restOrderActivityMockMvc = MockMvcBuilders.standaloneSetup(orderActivityResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static OrderActivity createEntity(EntityManager em) {
//        OrderActivity orderActivity = new OrderActivity()
//            .fromStatus(DEFAULT_FROM_STATUS)
//            .toStatus(DEFAULT_TO_STATUS)
//            .activityTime(DEFAULT_ACTIVITY_TIME)
//            .activityType(DEFAULT_ACTIVITY_TYPE)
//            .oldAmount(DEFAULT_OLD_AMOUNT)
//            .newAmount(DEFAULT_NEW_AMOUNT)
//            .oldItemName(DEFAULT_OLD_ITEM_NAME)
//            .newItemName(DEFAULT_NEW_ITEM_NAME);
//        return orderActivity;
//    }
//
//    @Before
//    public void initTest() {
//        orderActivity = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createOrderActivity() throws Exception {
//        int databaseSizeBeforeCreate = orderActivityRepository.findAll().size();
//
//        // Create the OrderActivity
//        OrderActivityDTO orderActivityDTO = orderActivityMapper.toDto(orderActivity);
//        restOrderActivityMockMvc.perform(post("/api/order-activities")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(orderActivityDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the OrderActivity in the database
//        List<OrderActivity> orderActivityList = orderActivityRepository.findAll();
//        assertThat(orderActivityList).hasSize(databaseSizeBeforeCreate + 1);
//        OrderActivity testOrderActivity = orderActivityList.get(orderActivityList.size() - 1);
//        assertThat(testOrderActivity.getFromStatus()).isEqualTo(DEFAULT_FROM_STATUS);
//        assertThat(testOrderActivity.getToStatus()).isEqualTo(DEFAULT_TO_STATUS);
//        assertThat(testOrderActivity.getActivityTime()).isEqualTo(DEFAULT_ACTIVITY_TIME);
//        assertThat(testOrderActivity.getActivityType()).isEqualTo(DEFAULT_ACTIVITY_TYPE);
//        assertThat(testOrderActivity.getOldAmount()).isEqualTo(DEFAULT_OLD_AMOUNT);
//        assertThat(testOrderActivity.getNewAmount()).isEqualTo(DEFAULT_NEW_AMOUNT);
//        assertThat(testOrderActivity.getOldItemName()).isEqualTo(DEFAULT_OLD_ITEM_NAME);
//        assertThat(testOrderActivity.getNewItemName()).isEqualTo(DEFAULT_NEW_ITEM_NAME);
//    }
//
//    @Test
//    @Transactional
//    public void createOrderActivityWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = orderActivityRepository.findAll().size();
//
//        // Create the OrderActivity with an existing ID
//        orderActivity.setId(1L);
//        OrderActivityDTO orderActivityDTO = orderActivityMapper.toDto(orderActivity);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restOrderActivityMockMvc.perform(post("/api/order-activities")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(orderActivityDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the OrderActivity in the database
//        List<OrderActivity> orderActivityList = orderActivityRepository.findAll();
//        assertThat(orderActivityList).hasSize(databaseSizeBeforeCreate);
//    }
//
//    @Test
//    @Transactional
//    public void getAllOrderActivities() throws Exception {
//        // Initialize the database
//        orderActivityRepository.saveAndFlush(orderActivity);
//
//        // Get all the orderActivityList
//        restOrderActivityMockMvc.perform(get("/api/order-activities?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(orderActivity.getId().intValue())))
//            .andExpect(jsonPath("$.[*].fromStatus").value(hasItem(DEFAULT_FROM_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].toStatus").value(hasItem(DEFAULT_TO_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].activityTime").value(hasItem(sameInstant(DEFAULT_ACTIVITY_TIME))))
//            .andExpect(jsonPath("$.[*].activityType").value(hasItem(DEFAULT_ACTIVITY_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].oldAmount").value(hasItem(DEFAULT_OLD_AMOUNT.doubleValue())))
//            .andExpect(jsonPath("$.[*].newAmount").value(hasItem(DEFAULT_NEW_AMOUNT.doubleValue())))
//            .andExpect(jsonPath("$.[*].oldItemName").value(hasItem(DEFAULT_OLD_ITEM_NAME.toString())))
//            .andExpect(jsonPath("$.[*].newItemName").value(hasItem(DEFAULT_NEW_ITEM_NAME.toString())));
//    }
//    
//    @Test
//    @Transactional
//    public void getOrderActivity() throws Exception {
//        // Initialize the database
//        orderActivityRepository.saveAndFlush(orderActivity);
//
//        // Get the orderActivity
//        restOrderActivityMockMvc.perform(get("/api/order-activities/{id}", orderActivity.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(orderActivity.getId().intValue()))
//            .andExpect(jsonPath("$.fromStatus").value(DEFAULT_FROM_STATUS.toString()))
//            .andExpect(jsonPath("$.toStatus").value(DEFAULT_TO_STATUS.toString()))
//            .andExpect(jsonPath("$.activityTime").value(sameInstant(DEFAULT_ACTIVITY_TIME)))
//            .andExpect(jsonPath("$.activityType").value(DEFAULT_ACTIVITY_TYPE.toString()))
//            .andExpect(jsonPath("$.oldAmount").value(DEFAULT_OLD_AMOUNT.doubleValue()))
//            .andExpect(jsonPath("$.newAmount").value(DEFAULT_NEW_AMOUNT.doubleValue()))
//            .andExpect(jsonPath("$.oldItemName").value(DEFAULT_OLD_ITEM_NAME.toString()))
//            .andExpect(jsonPath("$.newItemName").value(DEFAULT_NEW_ITEM_NAME.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingOrderActivity() throws Exception {
//        // Get the orderActivity
//        restOrderActivityMockMvc.perform(get("/api/order-activities/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateOrderActivity() throws Exception {
//        // Initialize the database
//        orderActivityRepository.saveAndFlush(orderActivity);
//
//        int databaseSizeBeforeUpdate = orderActivityRepository.findAll().size();
//
//        // Update the orderActivity
//        OrderActivity updatedOrderActivity = orderActivityRepository.findById(orderActivity.getId()).get();
//        // Disconnect from session so that the updates on updatedOrderActivity are not directly saved in db
//        em.detach(updatedOrderActivity);
//        updatedOrderActivity
//            .fromStatus(UPDATED_FROM_STATUS)
//            .toStatus(UPDATED_TO_STATUS)
//            .activityTime(UPDATED_ACTIVITY_TIME)
//            .activityType(UPDATED_ACTIVITY_TYPE)
//            .oldAmount(UPDATED_OLD_AMOUNT)
//            .newAmount(UPDATED_NEW_AMOUNT)
//            .oldItemName(UPDATED_OLD_ITEM_NAME)
//            .newItemName(UPDATED_NEW_ITEM_NAME);
//        OrderActivityDTO orderActivityDTO = orderActivityMapper.toDto(updatedOrderActivity);
//
//        restOrderActivityMockMvc.perform(put("/api/order-activities")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(orderActivityDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the OrderActivity in the database
//        List<OrderActivity> orderActivityList = orderActivityRepository.findAll();
//        assertThat(orderActivityList).hasSize(databaseSizeBeforeUpdate);
//        OrderActivity testOrderActivity = orderActivityList.get(orderActivityList.size() - 1);
//        assertThat(testOrderActivity.getFromStatus()).isEqualTo(UPDATED_FROM_STATUS);
//        assertThat(testOrderActivity.getToStatus()).isEqualTo(UPDATED_TO_STATUS);
//        assertThat(testOrderActivity.getActivityTime()).isEqualTo(UPDATED_ACTIVITY_TIME);
//        assertThat(testOrderActivity.getActivityType()).isEqualTo(UPDATED_ACTIVITY_TYPE);
//        assertThat(testOrderActivity.getOldAmount()).isEqualTo(UPDATED_OLD_AMOUNT);
//        assertThat(testOrderActivity.getNewAmount()).isEqualTo(UPDATED_NEW_AMOUNT);
//        assertThat(testOrderActivity.getOldItemName()).isEqualTo(UPDATED_OLD_ITEM_NAME);
//        assertThat(testOrderActivity.getNewItemName()).isEqualTo(UPDATED_NEW_ITEM_NAME);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingOrderActivity() throws Exception {
//        int databaseSizeBeforeUpdate = orderActivityRepository.findAll().size();
//
//        // Create the OrderActivity
//        OrderActivityDTO orderActivityDTO = orderActivityMapper.toDto(orderActivity);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restOrderActivityMockMvc.perform(put("/api/order-activities")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(orderActivityDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the OrderActivity in the database
//        List<OrderActivity> orderActivityList = orderActivityRepository.findAll();
//        assertThat(orderActivityList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteOrderActivity() throws Exception {
//        // Initialize the database
//        orderActivityRepository.saveAndFlush(orderActivity);
//
//        int databaseSizeBeforeDelete = orderActivityRepository.findAll().size();
//
//        // Delete the orderActivity
//        restOrderActivityMockMvc.perform(delete("/api/order-activities/{id}", orderActivity.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        // Validate the database is empty
//        List<OrderActivity> orderActivityList = orderActivityRepository.findAll();
//        assertThat(orderActivityList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(OrderActivity.class);
//        OrderActivity orderActivity1 = new OrderActivity();
//        orderActivity1.setId(1L);
//        OrderActivity orderActivity2 = new OrderActivity();
//        orderActivity2.setId(orderActivity1.getId());
//        assertThat(orderActivity1).isEqualTo(orderActivity2);
//        orderActivity2.setId(2L);
//        assertThat(orderActivity1).isNotEqualTo(orderActivity2);
//        orderActivity1.setId(null);
//        assertThat(orderActivity1).isNotEqualTo(orderActivity2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(OrderActivityDTO.class);
//        OrderActivityDTO orderActivityDTO1 = new OrderActivityDTO();
//        orderActivityDTO1.setId(1L);
//        OrderActivityDTO orderActivityDTO2 = new OrderActivityDTO();
//        assertThat(orderActivityDTO1).isNotEqualTo(orderActivityDTO2);
//        orderActivityDTO2.setId(orderActivityDTO1.getId());
//        assertThat(orderActivityDTO1).isEqualTo(orderActivityDTO2);
//        orderActivityDTO2.setId(2L);
//        assertThat(orderActivityDTO1).isNotEqualTo(orderActivityDTO2);
//        orderActivityDTO1.setId(null);
//        assertThat(orderActivityDTO1).isNotEqualTo(orderActivityDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(orderActivityMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(orderActivityMapper.fromId(null)).isNull();
//    }
}
